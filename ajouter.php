<?php
session_start();


// Connexion :
include_once("connexion_bdd.php") ;

try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("INSERT INTO commandes (id_menu, id_user) VALUES (?,?)");
   $statement->bindParam(1,$_POST["id_repas"]);
   $statement->bindParam(2,$_SESSION["user"]);
   $reussite = $statement->execute();

   if(!$reussite)
   {
        $_SESSION["ajout"]="Commande échouée" ;
        header("Location: menuplats1.php") ;
   }
   else
   {
        $_SESSION["ajout"]="Ajouter au panier" ;
        header("Location: menuplats1.php") ;
   }

   header('Location: menuplats1.php');
}
catch(PDOException $e)
{
   die("eeror".$e->getMessage());

}
$bd = null;
?>