<?php
$titre = "Page du panier";
include 'header.inc.php';
include 'menu.inc.php';
// Connexion :
include_once("connexion_bdd.php") ;
try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("SELECT menu.id,nom,description,prix,categorie,quantite,image FROM commandes INNER JOIN menu ON commandes.id_menu=menu.id WHERE id_user=?");
   $statement->bindParam(1,$_SESSION["user"]);
   $statement->execute();
   $commande = $statement->fetchAll();

}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>

<?php
 
?>
<section>
   <h2>Vôtre panier</h2>   
   <table class="table">
      <thead class="table-dark">
         <tr>
            <th>Image</th>
            <th>Nom</th>
            <th>Quantité</th>
            <th>Prix</th>
         </tr>
      </thead>
      <tbody>
      <?php
         $prixtotal=0;
       foreach($commande as $menu)// Pour récupérer les lignes de chaquue colonnes
       {
         $prixtotal += $menu[3] ;
      ?> 
         <tr>
            <td><img src="<?php echo($menu[6]); ?>" alt="" width="200" height="200"></td>
            <td><?php echo($menu[1]); ?></td>
            <td>
               <form id="form">
                  <input type="number" name="quantite" value="1">
               </form>
            </td>
            <td><?php echo($menu[3])."€";?></td>
            <td>
            <div class="d-grid gap-2 d-md-block">
               <form>
               <button class="btn btn-primary" formaction="supprimer.php" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Supprimer</button>
               <button class="btn btn-primary" form="form" formaction="valider.php" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Valider</button>
               </form>
            </div>
         </td>
         </tr>
       <?php
      }
      
       ?>
      </tbody>
      <tfoot>
         <tr>
            <td>Prix total :</td>
            <td><?php echo($prixtotal)."€";?></td>
         </tr>
      </tfoot>
   </table>
   <form action="valider.php" method="POST" class="d-flex align-items-center justify-content-center px-2">
                <label class="form-label label-heure" for="heure">Heure de retrait : </label>
                        <input type="time" class="form-control " name="heure" required/>
                <input class="btn btn-outline-danger mx-2" type="submit" name="save_date" value="VALIDER" />
</form>

   
   <div class="container">
         <form>
               <button class="btn btn-primary"formaction="paiement.php" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Paiement</button>
            </form>  
         </div>
</section>
<?php
  include 'footer.inc.php' ;
?>
