<?php
  $titre = "Page de connexion";
  include 'header.inc.php';
  include 'menu.inc.php';
  
?>
   <h1>Connexion</h1>
   <div class="container">
      <form class="row g-3" action="tt_connexion.php" method="post"> 
        <div class="col-md-6">
          <label for="mail" class="form-label">Email</label>
          <input type="email" class="form-control" id="mail" required name="l_email">
        </div>
        <div class="col-md-6">
          <label for="pass" class="form-label">Password</label>
          <input type="password" class="form-control" id="pass" required name="le_pass">
        </div>
        <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Connexion</button></div>   
    </div>
        
      </form>
    </div>
    <div class="grid text-center">
      <p>Vous n'avez pas encore de compte ?</p>
      <a href="http://localhost/page-web-restaurant/creation.php">Inscrivez vous ici</a>
    </div>

<?php 
  include 'footer.inc.php';
?> 