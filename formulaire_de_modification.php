<?php
  $titre = "Formulaire modification plat";
  include 'header.inc.php';
  include 'menu2.inc.php';
 
?>
    <h1>Modifier le plat</h1>
    <div class="container">
      <form class="row g-3" action="tt_modifier_plat.php" method="post"> 
        <div class="col-md-6">
          <label for="nom" class="form-label">Nom</label>
          <input type="text" class="form-control" id="nom" required name="le_nom">
        </div>
        <div class="col-md-6">
          <label for="prenom" class="form-label">Description</label>
          <input type="text" class="form-control" id="prenom" required name="la_description">
        </div>
        <div class="col-md-6">
          <label for="mail" class="form-label">Prix</label>
          <input type="text" class="form-control" id="prix" required name="le_prix">
        </div>
        <div class="col-md-6">
          <label for="pass" class="form-label">Categorie</label>
          <input type="text" class="form-control" id="categorie" required name="la_categorie">
        </div>
        <div class="col-md-6">
          <label for="pass" class="form-label">Image</label>
          <input type="text" class="form-control" id="image" required name="l_image">
        </div>
        <input type="text" value=<?php echo $_POST['id'] ?>  required name="id" hidden>
        <div class="row my-3">
          <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Envoyer</button></div>   
        </div>
      </form>
    </div>

<?php 
  include 'footer.inc.php';
?> 