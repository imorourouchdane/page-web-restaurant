<nav class="navbar navbar-expand-md navbar-dark bg-dark">
<img src="https://www.normandie-univ.fr/wp-content/uploads/sites/72/2020/06/Logo-ESIGELEC.jpg" width="50" height="50">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="index.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="commandes.php">Panier</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="platscommande.php">Commandes</a>
        </li>
      </ul>
      <ul class="navbar-nav mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="menuplats1.php">Menu</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="creation.php">Créer un compte</a>
          <i class="fa-solid fa-user"></i>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="connexion.php">Connexion</a>
        </li>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php">Deconnexion</a>
        </li>
      </div>
    </div>
  </div>
</nav>

