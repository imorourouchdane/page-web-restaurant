<nav class="navbar navbar-expand-md navbar-dark bg-dark">
<img src="https://www.normandie-univ.fr/wp-content/uploads/sites/72/2020/06/Logo-ESIGELEC.jpg" width="50" height="50">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index2.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="respo_restau_ajout_plat.php">Ajouter plats</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="menuplats2.php">Menu</a>
        </li>
      </ul>
      <ul class="navbar-nav mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="ajout_nouveau_respo.php">Ajout nouveau responsable</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="modifier_plat.php">Modifier plats</a>
          <i class="fa-solid fa-user"></i>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="supprimer_plat.php">Supprimer plats</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="platsvalides.php">Liste plats a preparer</a>
        </li>
      </div>
    </div>
  </div>
</nav>
