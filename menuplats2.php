<?php
// Connexion :
include_once("connexion_bdd.php") ;
try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->query("SELECT* FROM menu WHERE categorie = 'Entree' ");
   $statement1 = $bd->query("SELECT* FROM menu WHERE categorie = 'Resistance'");
   $statement2 = $bd->query("SELECT* FROM menu WHERE categorie = 'Dessert' ");
   $statement3 = $bd->query("SELECT* FROM menu WHERE categorie = 'Boisson' ");
   $nombre_de_menu = $statement->rowCount();

}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>

<?php
  $titre = "Menu";
  include 'header.inc.php';
  include 'menu2.inc.php';
?>
<section>
   <?php 
      if(isset($_SESSION["ajout"]))
      {
         echo("<script>alert(\"".$_SESSION["ajout"]."\"</script>");
         unset($_SESSION["ajout"]);
      }
   ?>
   <div class= "heading">
      <span>
         Nos Menus:
      </span>
   </div>
   <?php
       while($menu = $statement->fetch())// Pour récupérer les lignes de chaquue colonnes
       {
    ?>
         <div class="card" style="width: 18rem;">
            <img src="<?php echo($menu[6]); ?>" class="card-img-top" alt="">
            <div class="card-body">
            <h4><?php echo($menu[1]); ?></h4>
            <p><?php echo($menu[2]); ?></p>
            <h4><?php echo($menu[3])."€";?></h4>
            <form>
               <button formaction="ajouter.php" class="btn btn-outline-primary" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Ajouter au panier</button>
            </form> 
            </div>
         </div>  
   <?php
      }
   ?>

<?php
       while($menu = $statement1->fetch())// Pour récupérer les lignes de chaquue colonnes
       {
    ?>
         <div class="card" style="width: 18rem;">
            
            <img src="<?php echo($menu[6]); ?>" alt="">
            <div class="card-body">
            <h4><?php echo($menu[1]); ?></h4>
            <p><?php echo($menu[2]); ?></p>
            <h4><?php echo($menu[3])."€"; ?></h4>
            <form>
               <button formaction="ajouter.php" class="btn btn-outline-primary" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Ajouter au panier</button>
            </form> 
            </div>
       </div>   
   <?php
      }
   ?>

<?php
       while($menu = $statement2->fetch())// Pour récupérer les lignes de chaquue colonnes
       {
    ?>
         <div class="card" style="width: 18rem;">
            
            <img src="<?php echo($menu[6]); ?>" alt="">
            <div class="card-body">
            <h4><?php echo($menu[1]); ?></h4>
            <p><?php echo($menu[2]); ?></p>
            <h4><?php echo($menu[3]) ."€"; ?></h4>
            <form>   
               <button formaction="ajouter.php" class="btn btn-outline-primary" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Ajouter au panier</button>
            </form> 
            </div>

         </div>  

   <?php
      }
   ?>

<?php
       while($menu = $statement3->fetch())// Pour récupérer les lcolonnes de chaques lignes
       {
    ?>
         <div class="card" style="width: 18rem;">
            
            <img src="<?php echo($menu[6]); ?>" alt="">
            <div class="card-body">
            <h4><?php echo($menu[1]); ?></h4>
            <p><?php echo($menu[2]); ?></p>
            <h4><?php echo($menu[3]) ."€"; ?></h4>
            <form>
               <button formaction="ajouter.php" class="btn btn-outline-primary" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Ajouter au panier</button>
            </form>  
            </div> 
         </div>      
   <?php
      }
   ?>

</section>
<?php
  include 'footer.inc.php' ;
?>



