<?php
session_start();
// Connexion :
include_once("connexion_bdd.php") ;

try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("UPDATE commandes SET id_menu="", id_user="" WHERE id= (?,?)");
   $statement->bindParam(1,$_POST["id_repas"]);
   $statement->bindParam(2,$_SESSION["user"]);
   $reussite = $statement->execute();

   if(!$reussite)
   {
        $_SESSION["ajout"]="Commande échouée" ;
        header("Location: commandes.php") ;
   }
   else
   {
        $_SESSION["ajout"]="Ajouter au panier" ;
        header("Location: commandes.php") ;
   }

   header("Location: commandes.php") ;
}
catch(PDOException $e)
{
   die("eeror".$e->getMessage());

}
$bd = null;
?>