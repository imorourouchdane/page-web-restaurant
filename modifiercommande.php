<?php
$titre = "Page de modification commande";
include 'header.inc.php';
include 'menu.inc.php';
// Connexion :
include_once("connexion_bdd.php") ;
try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("SELECT menu.id,nom,description,prix,categorie,validation.quantite,image FROM validation INNER JOIN menu ON validation.id_menu=menu.id WHERE id_user=?");
   $statement->bindParam(1,$_SESSION["user"]);
   $statement->execute();
   $commande = $statement->fetchAll();

}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>

<?php
 
?>
<section>
   <h2>Vôtre panier</h2>   
   <table class="table">
      <thead class="table-dark">
         <tr>
            <th>Image</th>
            <th>Nom</th>
            <th>Quantité</th>
            <th>Prix</th>
         </tr>
      </thead>
      <tbody>
      <?php
         echo($_SESSION["user"]);
         $prixtotal=0;
       foreach($commande as $menu)// Pour récupérer les lignes de chaquue colonnes
       {
         $prixtotal += $menu[3] ;
      ?> 
         <tr>
            <td><img src="<?php echo($menu[6]); ?>" alt="" width="200" height="200"></td>
            <td><?php echo($menu[1]); ?></td>
            <td>
               <form id="form">
                  <input type="number" name="quantite" value="1">
               </form>
            </td>
            <td><?php echo($menu[3])."€";?></td>
            <td>
            <div class="d-grid gap-2 d-md-block">
               <form>
               <button class="btn btn-primary" formaction="tt_suppr_commande.php" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Supprimer</button>
               </form>
            </div>
         </td>
         </tr>
       <?php
      }
      
       ?>
      </tbody>
      <tfoot>
         <tr>
            <td>Prix total :</td>
            <td><?php echo($prixtotal)."€";?></td>
            <td><button class="btn btn-primary" form="form" formaction="tt_modif_commande.php" formmethod="POST" name="id_repas1" value="<?php echo($menu[0]); ?>" >Valider</button></td>
         </tr>
      </tfoot>
   </table>
   
</section>
<?php
  include 'footer.inc.php' ;
?>
