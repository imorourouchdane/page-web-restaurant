<?php
// Connexion :
include_once("connexion_bdd.php") ;
try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("SELECT menu.id,nom,description,prix,image FROM commandes INNER JOIN menu ON commandes.id_menu=menu.id WHERE id_user=");
   $statement->bindParam(1,$_SESSION["user"]);
   $statement->execute();
   $commande = $statement->fetchAll();

}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>

<?php
  $titre = "Panier";
  include 'header.inc.php';
  include 'menu.inc.php';
  $prixtotal=0;
?>
<section>

   <h2>Vôtre panier</h2>   
   <table>
      <thead>
         <tr>
            <th>image</th>
            <th>nom</th>
            <th>prix</th>
         </tr>
      </thead>
      <tbody>
      <?php
         echo($_SESSION["user"]);
       foreach($commande as $menu)// Pour récupérer les lignes de chaquue colonnes
       {
         $prixtotal +=  $menu[3] ;
      ?> 
         <tr>
            <td><img src="<?php echo($menu[4]); ?>" alt=""></td>
            <td><?php echo($menu[1]); ?></td>
            <td><?php echo($menu[3])."€";?></td>
         </tr>
       <?php
      }
       ?>
      </tbody>
      <tfoot>
         <tr>
            <td>Prix total</td>
            <td><?php echo($prixtotal)."€";?></td>
         </tr>
      </tfoot>
   </table>
   

</section>
<?php
  include 'footer.inc.php' ;
?>