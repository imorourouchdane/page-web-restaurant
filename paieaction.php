<?php
session_start();
// Connexion :
include_once("connexion_bdd.php") ;

$nomPrenom= htmlentities($_POST['nom_prenom']) ;
$email= htmlentities($_POST['le_mail']) ;
$adresse= htmlentities($_POST['ladresse']); 
$ville= htmlentities($_POST['la_ville']) ;
$pays= htmlentities($_POST['le_pays']) ;
$codeZip= htmlentities($_POST['le_code_zip']); 
$numeroC= htmlentities($_POST['le_numero_c']) ;
$nom= htmlentities($_POST['le_nom']) ;
$ccv= htmlentities($_POST['le_ccv']) ;
$date= htmlentities($_POST['le_date']); 


try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("INSERT INTO pay (nomprenom,email,adresse,ville,pays,code_zip,numero,nom_c,cvc,date) VALUES (?,?,?,?,?,?,?,?,?,?)");
   $statement->bindParam(1,$nomPrenom);
   $statement->bindParam(2,$email);
   $statement->bindParam(3,$adresse);
   $statement->bindParam(4,$ville);
   $statement->bindParam(5,$pays);
   $statement->bindParam(6,$codeZip);
   $statement->bindParam(7,$numeroC);
   $statement->bindParam(8,$nom);
   $statement->bindParam(9,$ccv);
   $statement->bindParam(10,$date);
   $validation= $statement->execute();

   

   if(!$validation)
   {
        $_SESSION["ajout1"]="Validation echouée" ;
   }
   else
   {
        $_SESSION["ajout1"]="Valider avec succès" ;
   }

   header("Location: index.php") ;
}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>