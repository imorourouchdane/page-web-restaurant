
<?php
   include 'header.inc.php';
  $titre = "Paiement";
  include 'menu.inc.php';
?>
<section>
   <div class="container">
      <form action="paieaction.php" method="post">
         <div class ="row">
            <div class="col">
               <h3>Adresse de facturation</h3>
               <div class="inputBox">
                  <span> Nom Prenom:</span>
                  <input type="text" placeholder=""  class="form-control" id="nom" required name="nom_prenom">
               </div>
               <div class="inputBox">
                  <span> Email :</span>
                  <input type="text" placeholder="" class="form-control" id="email" required name="le_mail">
               </div>
               <div class="inputBox">
                  <span> Adresse :</span>
                  <input type="text" placeholder="" class="form-control" id="adresse" required name="ladresse">
               </div>
               <div class="inputBox">
                  <span> Ville :</span>
                  <input type="text" placeholder="" class="form-control"id="ville" required name="la_ville">
               </div>
               <div class="inputBox">
                  <span> Pays :</span>
                  <input type="text" placeholder="" class="form-control" id="pays"required name="le_pays">
               </div>
               <div class="inputBox">
                  <span> Code zip :</span>
                  <input type="text" placeholder="" class="form-control"id="code" required name="le_code_zip">
               </div> 
            </div>
            <div class="col">
               <h3>Payment</h3>
               <div class="inputBox">
                  <span> Cartes acceptées:</span>
                  <img src="https://www.bing.com/images/search?view=detailV2&ccid=%2f5G%2ft%2fcD&id=2D565637D84E99C33EC03E7857451D33834F126C&thid=OIP._5G_t_cDd08gk9SumX5sdwHaBZ&mediaurl=https%3a%2f%2fwww.inrees.com%2fimg%2fcarte_acceptes.jpg&cdnurl=https%3a%2f%2fth.bing.com%2fth%2fid%2fR.ff91bfb7f703774f2093d4ae997e6c77%3frik%3dbBJPgzMdRVd4Pg%26pid%3dImgRaw%26r%3d0&exph=151&expw=800&q=cartes+accept%c3%a9es&simid=608013983477412757&FORM=IRPRST&ck=F197250E08D603481FADA0C85123038F&selectedIndex=1" alt="">
               </div>
               <div class="inputBox">
                  <span> Numéro de carte:</span>
                  <input type="text" placeholder="" class="form-control" id="numero"required name="le_numero_c">
               </div>
               <div class="inputBox">
                  <span> Nom de la carte:</span>
                  <input type="text" placeholder="" class="form-control" id="nomcarte"required name="le_nom">
               </div>
               <div class="inputBox">
                  <span> CVC/CCV:</span>
                  <input type="text" placeholder="" class="form-control" id="ccv" required name="le_ccv">
               </div>
               <div class="inputBox">
                  <span> Date d'expiration :</span>
                  <input type="text" placeholder="" class="form-control" id="date" required name="le_date">
               </div>
            </div>
         </div>
         <input type="submit" value="Payer" class="submit-btn" name="btn-valid"/>
      </form>
  </div>
</section>


<?php
  include 'footer.inc.php';
  
?>
