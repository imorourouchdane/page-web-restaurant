<?php
$titre = "Page du panier";
include 'header.inc.php';
include 'menu.inc.php';


$stmt = $mysqli->prepare("SELECT * FROM validation WHERE  id_user=?");
$stmt->bind_param("i",$_SESSION["user"]);
$stmt->execute();
$result=$stmt->get_result();
$link=$result->fetch_assoc();

     $_SESSION["prep"]= $link["preparation"];
     $_SESSION["pret"]= $link["prete"];

// Connexion :
include_once("connexion_bdd.php") ;
try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("SELECT menu.id,nom,description,prix,categorie,validation.quantite,image FROM validation INNER JOIN menu ON validation.id_menu=menu.id WHERE id_user=?");
   $statement->bindParam(1,$_SESSION["user"]);
   $statement->execute();
   $commande = $statement->fetchAll();

}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>

<?php
 
?>
<section>
   <h2>Vôtre panier</h2>   
   <table class="table">
      <thead class="table-dark">
         <tr>
            <th>Image</th>
            <th>Nom</th>
            <th>Quantité</th>
            <th>Prix</th>
         </tr>
      </thead>
      <tbody>
      <?php
         echo($_SESSION["user"]);
         $prixtotal=0;
       foreach($commande as $menu)// Pour récupérer les lignes de chaquue colonnes
       {
         $prixtotal += $menu[3] ;
      ?> 
         <tr>
            <td><img src="<?php echo($menu[6]); ?>" alt="" width="200" height="200"></td>
            <td><?php echo($menu[1]); ?></td>
            <td>
               <form id="form">
                  <input type="number" name="quantite" value="1">
               </form>
            </td>
            <td><?php echo($menu[3])."€";?></td>
         </tr>
       <?php
      }
      
       ?>
      </tbody>
      <tfoot>
         <tr>
            <td>Prix total :</td>
            <td><?php echo($prixtotal)."€";?></td>
            <td><button class="btn btn-primary" form="form" formaction="<?php
            if($_SESSION["prep"]==0)
             {
                echo("modifiercommande.php"); 
             }
             else if($_SESSION["pret"]==0)
             {
               echo("modifiercommande.php"); 
            }
             ?>" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >Modifier Commande</button></td>
         </tr>
      </tfoot>
   </table>
   

</section>
<?php
  include 'footer.inc.php' ;
?>
