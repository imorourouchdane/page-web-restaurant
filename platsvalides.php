<?php
$titre = "Page des plats a preparer";
include 'header.inc.php';
include 'menu2.inc.php';
// Connexion :
include_once("connexion_bdd.php") ;
try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("SELECT menu.id,nom,description,prix,categorie,validation.quantite,image FROM validation INNER JOIN menu ON validation.id_menu=menu.id WHERE id_user=2");
   //$statement->bindParam(1,$_SESSION["user"]);
   $statement->execute();
   $commande = $statement->fetchAll();

}
catch(PDOException $e)
{
   die("error".$e->getMessage());

}
$bd = null;
?>

<?php
 
?>
<section>
   <h2>Plats a preparer</h2>   
   <table class="table">
      <thead class="table-dark">
         <tr>
            <th>Image</th>
            <th>Nom</th>
            <th>Quantité</th>
            <th>Prix</th>
         </tr>
      </thead>
      <tbody>
      <?php
         $prixtotal=0;
       foreach($commande as $menu)// Pour récupérer les lignes de chaquue colonnes
       {
         $prixtotal += $menu[3] ;
      ?> 
         <tr>
            <td><img src="<?php echo($menu[6]); ?>" alt="" width="200" height="200"></td>
            <td><?php echo($menu[1]); ?></td>
            <td>
               <form id="form">
                  <input type="number" name="quantite" value="1">
               </form>
            </td>
            <td><?php echo($menu[3])."€";?></td>
         </tr>
       <?php
      }
      
       ?>
      </tbody>
      <tfoot>
         <tr>
            <td>Prix total :</td>
            <td><?php echo($prixtotal)."€";?></td>
            <td><button class="btn btn-primary" form="form" formaction="<?php
             if($_SESSION["prep"]==0)
             {
                echo("preparation.php"); 
             }
             else echo("prete.php"); ?>" formmethod="POST" name="id_repas" value="<?php echo($menu[0]); ?>" >
            <?php
             if($_SESSION["prep"]==0)
             {
                echo("Lancer la preparation") ;
             }
             else echo("Prete"); 
             ?></button></td>
         </tr>
      </tfoot>
   </table>
   

</section>
<?php
  include 'footer.inc.php' ;
?>
