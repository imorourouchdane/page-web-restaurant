<?php
session_start();
// contenu du formulaire
// Connexion :
include_once("connexion_bdd.php") ;

try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement = $bd->prepare("DELETE FROM commandes WHERE id_menu= ?");
   $statement->bindParam(1,$_POST["id_repas"]);
   
   $reussite = $statement->execute();

   if(!$reussite)
   {
        $_SESSION["ajout"]="Suppression echouée" ;
   }
   else
   {
        $_SESSION["ajout"]="Supprimer avec succès" ;
   }

   header("Location: commandes.php") ;
}
catch(PDOException $e)
{
   die("eeror".$e->getMessage());

}
$bd = null;
?>