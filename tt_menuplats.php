<?php
  session_start(); // Pour les messages

  
  // Contenu du formulaire :
  $nom =  htmlentities($_POST['le_nom']);
  $description = htmlentities($_POST['la_description']);
  $prix = htmlentities($_POST['le_prix']);
  $categorie =  htmlentities($_POST['la_categorie']);
  $image =  htmlentities($_POST['l_image']);
   // 0: pour l'utilisateur par défaut 1:...
  
  // Option pour bcrypt
  $options = [
        'cost' => 12,
  ];

  // Connexion :
  require_once("param.inc.php");
  $mysqli = new mysqli($host, $name, $passwd, $dbname);
  if ($mysqli->connect_error) {
      die('Erreur de connexion (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
  }

  // Attention, ici on ne vérifie pas si l'utilisateur existe déjà
  // Cette opération doit être faite, on suppose le nom comme étant
  // Un champ unique !
  if ($stmt = $mysqli->prepare("INSERT INTO menu(nom, description, prix, categorie, image) VALUES (?, ?, ?, ?, ?)")) {
    $stmt->bind_param("ssdss", $nom, $description,$prix, $categorie,$image);
    // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
    if($stmt->execute()) {
        $_SESSION['message'] = "Enregistrement réussi";
    } else {
        $_SESSION['message'] =  "Impossible d'enregistrer";
    }
  }
  // Redirection vers la page d'accueil 
  // Où le message présent dans la session sera affiché.
  header('Location: index2.php');

?>