<?php
 session_start(); // Pour les messages 


 // Contenu du formulaire :
  $nom =  htmlentities($_POST['le_nom']);
  $description = htmlentities($_POST['la_description']);
  $prix = htmlentities($_POST['le_prix']);
  $categorie =  htmlentities($_POST['la_categorie']);
  $image =  htmlentities($_POST['l_image']);
   


 // Connexion :
 require_once("param.inc.php");
 $mysqli = new mysqli($host, $name, $passwd, $dbname);
 if ($mysqli->connect_error) {
     die('Erreur de connexion (' . $mysqli->connect_errno . ') '
             . $mysqli->connect_error);
 }
 

 $stmt = $mysqli->prepare(" UPDATE menu SET nom = ?,description = ?,prix = ?,categorie = ?,image = ? WHERE  id = '$_POST[id]' ") ;
 $stmt->bind_param("ssdss", $nom, $description, $prix, $categorie, $image);
 

 $stmt->execute();

        if($stmt->execute()) {
            $_SESSION['message3'] = "Modification réussie";
        } else {
            $_SESSION['message3'] =  "Impossible de modifier";
        }
    
  // Redirection vers la page d'accueil 
  // Où le message présent dans la session sera affiché.
  header('Location: modifier_plat.php');
  