<?php
session_start();

// Contenu du formulaire :
   $heureRetrait =  htmlentities($_POST['save_date']);
   $prete=0;
   $preparation = 0; 

// Connexion :
include_once("connexion_bdd.php") ;

try
{
   $bd = new PDO($server,$identifiant,$motDePasse);
   $statement1 = $bd->prepare("INSERT INTO validation (id_menu, id_user, quantite,heure_retrait,preparation,prete) VALUES (?,?,?,?,?,?)");
   $statement1->bindParam(1,$_POST["id_repas"]);
   $statement1->bindParam(2,$_SESSION["user"]);
   $statement1->bindParam(3,$_POST["quantite"]);
   $statement1->bindParam(4,$heureRetrait);
   $statement1->bindParam(5,$preparation);
   $statement1->bindParam(6,$prete);
   $statement2 = $bd->prepare("DELETE FROM commandes WHERE  id_menu = '$_POST[id_repas]' ");
   $reussite = $statement1->execute();
   $statement2->execute();

   if(!$reussite)
   {
        $_SESSION["ajout"]="Validation echouée" ;
   }
   else
   {
        $_SESSION["ajout"]="Valider avec succès" ;
   }

   header("Location: commandes.php") ;
}
catch(PDOException $e)
{
   die("eeror".$e->getMessage());

}
$bd = null;
?>